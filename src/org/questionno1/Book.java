package org.questionno1;

public abstract class Book {
	abstract void write();

	abstract void read();

}
