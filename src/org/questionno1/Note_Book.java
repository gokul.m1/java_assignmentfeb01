package org.questionno1;

public class Note_Book extends Book {
	public void draw() {

		System.out.println("Draw the picture.");

	}

	@Override
	void write() {
		System.out.println("To write the paragraph.");

	}

	@Override
	void read() {

		System.out.println("To read the book.");

	}

	public static void main(String[] args) {

		Note_Book n = new Note_Book();
		n.draw();
		n.write();
		n.read();
	}

}
