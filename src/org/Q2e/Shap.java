package org.Q2e;

public abstract class Shap {
	public abstract void RectangleArea(int len, int breth);

	public abstract void SquareArea(int side);

	public abstract void CircleArea(double radius);

}
